// console.log(`Process Architecture: ${process.arch}`);  
// console.log(`Process PID: ${process.pid}`);  
// console.log(`Process Platform: ${process.platform}`);  
// console.log(`Process Version: ${process.version}`);  

//example 2
// process.argv.forEach((value, index, array) => {  
//     console.log(`${index}: ${value}`);  
//   });  

//example 3
console.log(`Current directory: ${process.cwd()}`);  
console.log(`Uptime: ${process.uptime()}`);  
console.log(`hrtime: ${process.hrtime()}`);  
console.log(`Memory usage: ${process.memoryUsage()}`);  