
// setImmediate(): It is used to execute setImmediate.
// setInterval(): It is used to define a time interval.
// setTimeout(): ()- It is used to execute a one-time callback after delay milliseconds.
// Clear timer functions:

// clearImmediate(immediateObject): It is used to stop an immediateObject, as created by setImmediate
// clearInterval(intervalObject): It is used to stop an intervalObject, as created by setInterval
// clearTimeout(timeoutObject): It prevents a timeoutObject, as created by setTimeout


// Set Interval
// setInterval(function() {  
//     console.log("setInterval: Hey! 1 millisecond completed!..");   
//    }, 2000);        

var i =0;  
console.log(i);  
setInterval(function(){  
i++;  
console.log(i);  
}, 1000);   

//Set Timeout
// setTimeout(function() {   
//     console.log("setTimeout: Hey! 1000 millisecond completed!..");  
//     }, 4000);  
// var recursive = function () {  
//     console.log("Hey! 1000 millisecond completed!..");   
//     setTimeout(recursive,1000);  
// }  
// recursive();    

//Clear timeout and interval
  // function welcome () {  
  //     console.log("Welcome to JavaTpoint!");  
  //   }  
  //   var id1 = setTimeout(welcome,1000);  
  //   var id2 = setInterval(welcome,1000);  
  //   //clearTimeout(id1);  
  //   clearInterval(id2);  

  // //Errors
  // try {  
  //   const a = 1;  
  //   const c = a + b;  
  // } catch (err) {  
  //   console.log(err);  
  // }  