const express = require ('express');
const router = express.Router();


//multer
const uploadMulter = require('../middleware/uploads.js')
const validation = require('../validation/validation.js')

//controller
const {createUpload} = require('../controller/uploadController.js');

router.post('/uploadImage',uploadMulter, validation, createUpload);

module.exports = router