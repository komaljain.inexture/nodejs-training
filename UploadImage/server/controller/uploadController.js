const Image = require('../model/model.js');

exports.createUpload = (req, res) => {
   const name = req.body.name
   const image = req.file.path

   const upload = new Image({
       name: name,
       image: image
   })
   upload.save((err,upload)=>{
       if(err){
           return res.status(404).json({
               errors: err.message
           })
       }
       return res.json({
           message: 'Successful created Image',
           upload
       })
   })
}