require('./models/db');

const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
// const bodyparser = require('body-parser');
const Handlebars =require('handlebars')
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')

const employeeController = require('./controllers/employeeController');

var app = express();
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.set('views', path.join(__dirname, '/views/'));

app.engine('handlebars', exphbs({
defaultLayout: 'mainLayout', 
layoutsDir: __dirname + '/views/layouts/',
handlebars: allowInsecurePrototypeAccess(Handlebars) }));
app.set('view engine', 'handlebars');

app.listen(4000, () => {
    console.log('Express server started at port : 4000');
});

app.use('/employee', employeeController);