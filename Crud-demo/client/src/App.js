import React, { useState } from 'react';
import './App.css';
import Axios from 'axios';



function App(){
    const [fullName,setFullName] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const addToList =() =>{
        //Axios.post("http://localhost:4000/employee",{fullName: fullName , email:email , password:password})
        
    };
    return(
        <div className='App'>
            <h1>Crud App</h1>
            <form>
                <input type="hidden" name="_id" />
                <div className="form-group">
                    <label>Full Name</label>
                    <input type="text" className="form-control" name="fullName" placeholder="Full Name" 
                    onChange={(event)=>{setFullName(event.target.value)}}/>
                </div>
                <div className="form-group">
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email" 
                    onChange={(event)=>{setEmail(event.target.value)}}/>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="text" className="form-control" name="password" placeholder="Password" 
                    onChange={(event)=>{setPassword(event.target.value)}}/>
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-info" onClick={addToList}><i className="fa fa-database"></i> Submit</button>
                </div>
            </form>
            
        </div>
        
    );
}
export default App