const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const members= require('./data/Members')

const logger = require ('./middleware/logger');

const app = express();
// app.get('/',(req,res)=>{
//     res.send('<h1>Hello world!!!</h1>');
// });
// app.get('/about',(req,res)=>{
//     res.send('<h1>Hello About!</h1>');
// });

//Init middleware
//app.use(logger);

// Handlebars Middleware
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

//Init body parser middleware
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// Homepage Route
app.get('/', (req, res) =>
  res.render('index', {
    title: 'Member App',
    members
  })
);

//set static folder
app.use(express.static(path.join(__dirname,'public')));

app.use('/api/members', require('./routes/members'));

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>  console.log(`Server started on port ${PORT}`));