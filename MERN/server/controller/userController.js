const user = require('../model/userModel');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");


exports.addUsers = async (req, res) => {
  const { name, email, password, phone } = req.body;
  const image = req.file.path;

  console.log(req.body);
  console.log(req.file.path)


  if (!name || !email || !password || !phone)
    return res.json({ message: "Invalid Credentials" });
  const userExists = await user.findOne({ name: name });

  if (userExists) return res.json({ message: "User already exists" });
  else {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const newUser = new user({
      name,
      email,
      password: hashPassword,
      phone,
      image

    });
    newUser.save();
    return res.json({ message: "User Created", newUser });
  }
};
exports.getLogin = async (req, res) => {
  const { email, password } = req.body.data;

  if (!email || !password)
    return res.json({ message: "Invalid Credentials" });

  const users = await user.findOne({ email: email });

  if (users) {
    const matchPassword = await bcrypt.compare(password, users.password);
    if (matchPassword) {
      const payload = {
        email,
      };
      jwt.sign(payload, "secret", { expiresIn: "1d" }, (err, token) => {
        if (err) console.log(err);
        else {
          res.cookie("jwt", token);
          return res.json({ message: "User logged In", token: token });
        }
      });
    } else {
      return res.json({ message: "Incorrect Password" });
    }
  } else {
    return res.json({ message: "Incorrect Credentials" });
  }
};

exports.getUsersList = async (req, res) => {
  try {
    const User = await user.find();
    res.json(User);
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.deleteUser = async (req, res) => {
  try {
    await user.deleteOne({ _id: req.params.id });
    res.json("User deleted ");
  } catch (error) {
    res.json({ message: error.message });
  }

}
exports.editUser = async (req, res) => {
  const { name, email, phone } = req.body;
  const image = req.file.path;
  // const editUser = new User(user);
  console.log(image);
  try {
    const updatedUser = await user.findOneAndUpdate(
      { _id: req.params.id },
      { name, email, phone,image },
      { new: true }
    );
    res.json(updatedUser);
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getUserById = async (req, res) => {
  const id = req.params.id; 
  try {
    const User = await user.findById(id);
    res.json(User);
  } catch (error) {
    res.json({ message: error.message });
  }
}

