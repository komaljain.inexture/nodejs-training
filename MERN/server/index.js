const express = require('express');
const app = express();
const mongoose = require('mongoose');
const url = require('./model/db').url;
const cors = require('cors');
const bodyParser = require('body-parser')
// const route = require('./routes/route')
const path = require('path');


app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors());
// app.use('/user',route);

mongoose.set("useFindAndModify",false);
mongoose.connect(url,{useNewUrlParser:true,
    useUnifiedTopology:true})
    .then(()=>console.log('MongoDB connected'))
    .catch(err=>{
    console.log(err)
    });
const PORT = 4000;

app.use('/user',require('./routes/route.js'));
// app.use('/uploads', express.static('uploads'))
app.use('/uploads',express.static(path.join(__dirname, "./uploads")));

app.listen(PORT,()=>{
    console.log(`Server started at ${PORT}`);
});