const express = require('express');
const { addUsers, getLogin, getUsersList, deleteUser, editUser, getUserById } = require('../controller/userController');

const route = express.Router();
const uploadMulter = require('../middleware/upload.js')
const validation = require('../validation/validation.js')



route.post('/signup', uploadMulter, validation, addUsers);
route.post('/login', getLogin)
route.get('/alluser', getUsersList)
route.delete('/:id', deleteUser);
route.put('/:id', uploadMulter,validation,editUser);
route.get('/:id', getUserById);

module.exports = route;