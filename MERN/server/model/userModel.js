const mongoose = require('mongoose');
const autoInc = require('mongoose-auto-increment');

const userModel = mongoose.Schema({
    name: {
        type: String,
        required: true
    }, 
    email:{
        type: String,
        required:true
    },
    password: {
        type: String,
        required:true
    },
    phone: {
        type: Number,
        required:true
    },
    image: {
        type: String,
        trim: true,
        required: true
    }
}, {
    timestamps: true
   
});
autoInc.initialize(mongoose.connection);
userModel.plugin(autoInc.plugin,"user")
const user = mongoose.model("user",userModel);
module.exports = user;