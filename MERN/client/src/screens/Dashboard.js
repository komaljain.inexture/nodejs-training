import React, { lazy, Suspense } from 'react';
//import { useHistory} from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { UserLogout } from "../reducer/actions";
// import AllUsers from './AllUsers'
import Loading from '../components/loading'


const AllUsers = lazy(() => {
  return new Promise(resolve => {
    setTimeout(() => resolve(import("./AllUsers")), 1000);
  });
});


const Dashboard = () => {
  const dispatch = useDispatch();

  // Remove data after logout
  function remove() {
    dispatch(UserLogout());

  }
  // Dashboard Nav
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <h1 className="navbar-brand" >DASHBOARD</h1>
          <h5> <button type="submit" className="btn_logout" onClick={remove}>Logout</button></h5>
         
        </div>
      </nav>
      <Suspense fallback={<Loading />}>
        <AllUsers />
      </Suspense>
    </div>
  );

};
const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  UserLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
  ;
