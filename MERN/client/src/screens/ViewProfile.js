
import "./ViewProfile.css";
import { Button } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { getUserById } from "../reducer/actions";

function ViewProfile() {
  const [user, setUsers] = useState([]);
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    loadUserDetails();
  }, []);

  const loadUserDetails = async () => {
    const response = await getUserById(id);
    setUsers(response.data);
  };

  const handleProfile = async () => {
    history.push("/dashboard");
  };
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <h3 className="navbar-brand" >USER PROFILE</h3>
        </div>
      </nav>
      <div className="card" style={{ marginTop: "15vh" }}>
        <img
          src={`http://localhost:4000/${user.image}`}
          style={{ width: "300px", height: '220px' }}
        />
        <p style={{ fontSize: '20px', color: 'black', fontWeight: 'bold' }}>Name: {user.name}</p>
        <p style={{ fontSize: '17px', color: 'maroon' }}>Phone: {user.phone}</p>
        <p style={{ fontSize: '17px', color: 'maroon' }}>Email: {user.email}</p>

        <div style={{ margin: "15px" }}></div>
        <p>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => handleProfile()}
          >
            back
          </Button>
        </p>
      </div>

    </div>
  );
}

export default ViewProfile;