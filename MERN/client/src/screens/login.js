import React, { useState } from "react";
import { useForm } from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {Link} from 'react-router-dom';
import {schema} from '../Validations/LoginValidation';
import { connect, useDispatch } from 'react-redux';
import { CallUserLogin } from '../reducer/actions';




 function Login(){
   
    const { register, formState: { errors }, handleSubmit } = useForm({
        resolver : yupResolver(schema),
        reValidateMode:"onChange",
        mode:"all"});
    const dispatch =useDispatch();    
   
    const [email,setEmail]=useState("");
    const [password,setPassword]=useState("");

    // On submit login details 
    const onSubmit = (data) =>{
       
        dispatch(CallUserLogin(data));
        };
      
        const handleMail =(e)=>{
            setEmail(e.target.value);

        };
        const handlePassword =(e)=>{
            setPassword(e.target.value);

        };

    
//   Login Form
        return (
            <div className="outer">
            <form className="box" onSubmit={handleSubmit(onSubmit)}>

                <h3>Log In</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input {...register("email")}
                      type="email" className="form-control" placeholder="Enter email" name="email"
                      value={email}
                      onChange={handleMail}/>
                    <p>{errors?.email?.message}</p>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input {...register("password")}
                   
                   type="password" className="form-control" placeholder="Enter password" name="password"
                   value={password}
                   onChange={handlePassword}
                  
                  />
                   <p>{errors?.password?.message}</p>
                  
                 
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block" >Login</button>
                <p className="forgot-password text-right">
                    Not Registered Yet <Link to="/sign-up">Register?</Link>
                </p>
            </form>
            </div>
        );
    }
    const mapStateToProps = (state) => {
        return {
          user: state,
        };
      };
      
      const mapDispatchToProps = {
        CallUserLogin,
      };
      
      export default connect(mapStateToProps, mapDispatchToProps)(Login);