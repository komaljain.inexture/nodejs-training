import { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { editUser, getUserById } from "../reducer/actions";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schema } from '../Validations/validation'

const initialValue = {
  name: "",
  email: "",
  phone: ""

};
const EditUser = () => {
  const [User, setUser] = useState(initialValue);
  const { name, email, phone } = User;
  const [uploadFile, setUploadFile] = useState();
  const { id } = useParams();

  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    reValidateMode: "onChange",
    mode: "all",
  });
  const onSubmit = (User) => console.log(User);

  useEffect(() => {
    loadUserDetails();
  }, []);

  const loadUserDetails = async () => {
    const response = await getUserById(id);
    setUploadFile(response.data)
    setUser(response.data);
    console.log(response.data)
    // console.log(setUploadFile)
  };

  const editUserDetails = async () => {
    const response = await editUser(id, User);
    const formData = new FormData();
    formData.append("image", uploadFile);
    history.push("/Dashboard");
  };
  const onProfileChange = (e) => {
    console.log(e.target.files[0]);
    // setUploadFile(files[0])
    setUploadFile({[e.target.Files]: e.target.files[0]})
  }
  const onValueChange = (e) => {
    console.log(e.target.value);
    setUser({
      ...User,[e.target.name]: e.target.value
      });
  };

  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <h1 className="navbar-brand" >Edit User</h1>
        </div>
      </nav>
      <form className="box" onSubmit={handleSubmit(onSubmit)} encType='multipart/form-data' >
        <h2>Edit Information</h2>
        <div className="form-group">
          <label className="label">Name</label><br />
          <input  {...register("name")}
            type="text" id="name" className="form" placeholder="Name" name="name" value={name}
            onChange={(e) => onValueChange(e)}
          />
          <p>{errors?.name?.message}</p>
        </div>


        <div className="form-group">
          <label className="label">Email</label><br />
          <input  {...register("email")}
            type="email" className="form" placeholder=" Email" name="email" value={email}
            onChange={(e) => onValueChange(e)}
          />
          <p>{errors?.email?.message}</p>

        </div>

        <div className="form-group">
          <label className="label">Phone</label><br />
          <input {...register("phone")}
            type="number" className="form" placeholder="Phone" name="phone" value={phone}
            onChange={(e) => onValueChange(e)}
          />
          <p>{errors?.phone?.message}</p>
        </div>
        <div className='form-group'>
          <label >
            Upload Image
          </label>
          <input
            type='file'
            className='custom-file-input'
            id='inputGroupFile04'
            aria-describedby='inputGroupFileAddon04'
            onChange={(e) => onProfileChange(e)}
            
            
          />

        </div>
        <button type="submit" className="btn btn-dark btn-lg btn-block" onClick={() => editUserDetails()} >  Edit User</button>
      </form>
    </div>
  );
};

export default EditUser;