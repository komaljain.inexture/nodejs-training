import React, { useState, useEffect  } from "react";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { getUsersList, deleteUser } from "../reducer/actions";
import { Link } from "react-router-dom";


const useStyles = makeStyles({
  table: {
    maxWidth: '80%',
    margin: "60px 0 0 130px",
  },
  thead: {
    "& > *": {
      fontSize: 20,
      background: "#735C56",
      color: "#FFFFFF",
    },
  },
  row: {
    "& > *": {
      fontSize: 15,
    },
  },
});

const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    getAllUsers();
  }, []);

  const deleteUserData = async (id) => {
    await deleteUser(id);
    getAllUsers();
  };

  const getAllUsers = async () => {
    let response = await getUsersList();
    setUsers(response.data);
  };

  return (
    <Table className={classes.table}>
      <TableHead>
        <TableRow className={classes.thead}>
          <TableCell>Id</TableCell>
          <TableCell>Name</TableCell>
          <TableCell>Email</TableCell>
          {/* <TableCell>Password</TableCell> */}
          <TableCell>Phone</TableCell>
          <TableCell>Profile</TableCell>
          <TableCell>Actions</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {users.map((User) => (
          <TableRow className={classes.row} key={User.id}>
            <TableCell>{User._id}</TableCell>
            <TableCell>{User.name}</TableCell>
            <TableCell>{User.email}</TableCell>
            <TableCell>{User.phone}</TableCell>
            <TableCell> 
              <img
              src={`http://localhost:4000/${User.image}`}
              style={{ width: "50px", margintop: "20px" }}
              />
            </TableCell>
            <TableCell>
              <Button
                variant="contained"
                style={{ marginRight: 10 }}
                component={Link}
                to={`/edit/${User._id}`}
              >
              <i className='fas fa-edit' style={{ fontSize: '20px' }}></i>
              </Button>
              <Button
                variant="contained"
                style={{ width: 20 }}
                component={Link}
                // onClick={() => deleteUserData(User._id)}
                onClick={() => {
                  const confirmBox = window.confirm(
                  "Do you really want to Delete" + User.name + "?")
                  if (confirmBox === true) {
                    deleteUserData(User._id)
                  }
                }}
              >
                <i className='fas fa-trash-alt' style={{ fontSize: '20px' }}></i>
              </Button>
              <Button
                variant="contained"
                style={{ width: 20, marginLeft: 10 }}
                component={Link}
                to={`/view/${User._id}`}
              >
                <i className='fas fa-eye' style={{ fontSize: '20px' }}></i>
              </Button>
            </TableCell>
          </TableRow>
        ))}
       
      </TableBody>
    
    </Table>
  );
};

export default AllUsers;