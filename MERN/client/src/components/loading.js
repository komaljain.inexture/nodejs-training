import React from 'react';
// import { BeatLoader } from "react-spinners";
import img from '../loader1.gif'

const Loading = () => (

    <div style={{ textAlign: 'center',marginTop:'100px' }}>
        <img src={img} alt='loading' />
    </div>
    // return <BeatLoader color="blue" size={72}  margin={90} marginLeft={50}/>;
);
export default Loading;
