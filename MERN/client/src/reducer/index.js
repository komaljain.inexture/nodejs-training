import { USER_REGISTER, USER_LOGIN, LOGOUT} from "./types";

const initialState = {
  newUser: {},
  isUserRegister: false,
  isUserLogin: false,
  
 
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER:
      return {
        ...state,
        newUser: action?.user,
      };
    case USER_LOGIN:
      return {
        ...state,
        newUser: {}
      };
    case LOGOUT:
      return {
        newUser: {},
        ...initialState
      }
   
    default:
      return state;
  }
};
