import { USER_REGISTER, LOGOUT } from "./types";
import history from "./history";
import axios from "axios";
import Cookie from "js-cookie";

const usersUrl = 'http://localhost:4000/user';


export function setUserRegister(user) {
  console.log("setUserRegister");
  return {
    type: USER_REGISTER,
    user,
  };
}

export function CallUserRegister(data) {
  return async (dispatch) => {
    dispatch(setUserRegister(data));
    axios
      .post('http://localhost:4000/user/signup', data)
      .then((res) => {
        console.log(res.data);
        setTimeout(() => {
        }, 1000);
      })
      .then((response) => {
        console.log("heloooooooooooooo");
        alert("You've Registered successfully");
        history.push('/sign-in')
      })
      .catch((error) => {
        console.log(error.response);
        console.log("chalooooooooooo");
      });

    //     const res = await axios.post("http://localhost:4000/user/signup", 
    //     {data}
    //       );
    //       console.log(res.data);
  };
}

export async function CallUserLogin(data) {
  const res = await axios.post("http://localhost:4000/user/login", {
    data,
  });
  if (res.data.token) {
    Cookie.set("jwt", res.data.token);
    history.push("/dashboard");
    console.log(res.data.token);
  } else {
    history.push("/sign-in");
    alert("Invalid email or password");
  }

}

export const getUsersList = async (id) => {
  id = id || '';
  return await axios.get("http://localhost:4000/user/alluser"); 
}

export const deleteUser = async (id) => {
  return await axios.delete(`${usersUrl}/${id}`);
}
export const editUser = async (id, User) => {
  return await axios.put(`${usersUrl}/${id}`, User)
  // return await axios.put(`http://localhost:4000/edit/${id}`,User)
}
export const getUserById = async (id) => {
  return await axios.get(`${usersUrl}/${id}`)

}

export function UserLogout() {
  console.log("bye");
  history.push("/sign-up");
  return {
    type: LOGOUT,
  };
}

