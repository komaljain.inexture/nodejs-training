import * as yup from "yup";

export const schema =yup.object().shape({
    email: yup.string().required("Please enter valid email").email(),
    password: yup.string().required("Password must contain 5 to 15 character").min(5).max(15)

});