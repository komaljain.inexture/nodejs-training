import * as yup from "yup";

export const schema = yup.object().shape({
    name: yup.string().required("Name is required").min(5),
    // username: yup.string().required("Username is required").min(5),
    email: yup.string().required("Please enter valid Email").email(),
    password: yup.string().required("Password must contain 5 to 15 character").min(5).max(15),
    phone: yup.string().required("Phone Number is required").max(10)
  })