import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {Router as Router, Switch, Route ,Link} from "react-router-dom";
import Login from "./screens/login";
import SignUp from "./screens/Signup";
import Dashboard from "./screens/Dashboard";
// import ProtectedRoute from './components/ProtectedRoutes';
import EditUser from './screens/EditUser';
import history from './reducer/history'
import ViewProfile from './screens/ViewProfile';

function App(){
  
 return (<>
    {/* --------------Nav bar--------------------- */}
          <Router history={history}> 
            <div className="App">
                <nav className="navbar navbar-expand-lg navbar-light fixed-top">
               <div className="container">
                <Link className="navbar-brand" to={"/sign-up"}>React App</Link>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul className="navbar-nav ml-auto">
              
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-in"}>Login</Link>
                  </li>
                 
                 
                </ul>
              </div>
            </div>
          </nav>
    
   
          </div> 
          <Switch>
            <Route exact path='/sign-up'  component={SignUp} />
            <Route path="/sign-in"   component={Login} />
            <Route  path="/Dashboard" component={Dashboard} />  
            <Route path="/edit/:id" component={EditUser} />
            <Route exact path="/view/:id" component={ViewProfile} />
            {/* <Route  path='/DetailPage' component={DetailPage} />  */}
          </Switch>
    
    </Router>
    
    
   </>
   

    
  );
}

export default App;