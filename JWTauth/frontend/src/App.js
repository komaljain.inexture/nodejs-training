import "./App.css";
import { useState } from "react";
import axios from "axios";
import Cookie from "js-cookie";

function App() {
  const [userData, setUserData] = useState({});

  const handleChange = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
    console.log(userData);
  };

  const handleSignup = async (e) => {
    e.preventDefault();
    const res = await axios.post("http://localhost:4000/auth/signup", {
      data: userData,
    });
    console.log(res.data);
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    const res = await axios.post("http://localhost:4000/auth/login", {
      data: userData,
    });
    if (res.data.token) Cookie.set("jwt", res.data.token);
    console.log(res.data.token);
  };

  const handleProtected = async (e) => {
    const headers = {
      authorization: `Bearer ${Cookie.get("jwt")}`,
    };
    const res = await axios.get("http://localhost:4000/auth/protected",
      {headers},
    );
    console.log(res.data);
  };

  return (
    <div className="App">
      <form action="" onChange={handleChange}>
        <br />
        <br />
        <input type="text" placeholder="username" name="username" />
        <br />
        <br />
        <input type="password" placeholder="password" name="password" />
        <br />
        <br />
        <button type="submit" onClick={handleSignup}>
          signup
        </button>
        <button type="submit" onClick={handleLogin}>
          login
        </button><br/>
      </form>
      <button onClick={handleProtected}>Access Protected </button>
    </div>
  );
}

export default App;