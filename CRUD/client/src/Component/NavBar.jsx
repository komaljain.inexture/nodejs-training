import { AppBar, Toolbar, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyle = makeStyles({
    header: {
        background: '#6F6360'
    },
    tabs: {
        color: '#FFFFFF',
        marginRight: 20,
        textDecoration: 'none',
        fontSize: 20,
        
    }
    
})

const NavBar = () => {
    const classes = useStyle();
    return (
        <AppBar position="static" className={classes.header}>
            <Toolbar>
                {/* <NavLink className={classes.tabs} to="./" exact>Dashboard</NavLink> */}
                <NavLink className={classes.tabs} to="./" exact>Login</NavLink>
                <NavLink className={classes.tabs} to="add" exact>Add User</NavLink>
                <NavLink className={classes.tabs} to="all" exact>Users List</NavLink>
            </Toolbar>
        </AppBar>
    )
}

export default NavBar;