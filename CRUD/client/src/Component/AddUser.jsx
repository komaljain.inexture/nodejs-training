import  { useState } from 'react';
import { addUser } from '../Service/api';
import { useHistory } from 'react-router-dom';
import { useForm } from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import { schema } from '../Validation/validation';



const initialValue = {
    name: '',
    username: '',
    email: '',
    password:'',
    phone: ''
}



const AddUser = () => {
    const [user, setUser] = useState(initialValue);
    const { name, username, email, password, phone } = user;
    const history = useHistory();

    const {register ,handleSubmit,formState:{errors}} = useForm({ resolver : yupResolver(schema),
        reValidateMode:"onChange",
        mode:"all"});
    const onSubmit = (user) => console.log(user);

    const onValueChange = (e) => {
        console.log(e.target.value);
        setUser({...user, [e.target.name]: e.target.value})
    }

    const addUserDetails = async() => {
        await addUser(user);
        history.push('./all');
    }
   
    return (
       
        <div className="App">
    <form className="box" onSubmit={handleSubmit(onSubmit)} >
        <h2>Add User</h2>
                <div className="form-group">
                    <label className="label">Name</label><br />
                    <input  {...register("name")}
                    type="text" id="name" className="form"  placeholder="Name" name="name" value={name}
                    onChange={(e) => onValueChange(e)}
                     />
                     <p>{errors?.name?.message}</p>
                </div>
                <div className="form-group">
                    <label  className="label">Username</label><br />
                    <input  {...register("username")}
                    type="text" id="username" className="form"  placeholder="Username" name="username" value={username}
                    onChange={(e) => onValueChange(e)}
                     />
                     <p>{errors?.username?.message}</p>
                </div>

                <div className="form-group">
                    <label  className="label">Email</label><br />
                    <input  {...register("email")}
                     type="email" className="form" placeholder=" email" name="email" value={email}
                     onChange={(e) => onValueChange(e)}
                  />
                   <p>{errors?.email?.message}</p>
               
                </div>

                <div className="form-group">
                    <label  className="label">Password</label><br />
                    <input {...register("password")}
                    type="password" className="form" placeholder="Password" name ="password" value={password}
                    onChange={(e) => onValueChange(e)}
                   />
                    <p>{errors?.password?.message}</p>
                </div>
                <div className="form-group">
                    <label  className="label">Phone</label><br />
                    <input {...register("phone")}
                    type="number" className="form" placeholder="Phone" name ="phone" value={phone}
                    onChange={(e) => onValueChange(e)}
                   />
                    <p>{errors?.phone?.message}</p>
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block" onClick={() => addUserDetails()} >  Add User</button>
             
               
    </form>
    </div>
    )
}

export default AddUser;