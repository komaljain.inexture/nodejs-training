//  import express from "express";
// import mongoose from "mongoose";
// import route from "./route/routes.js";
// import cors from 'cors';
// import bodyParser from "body-parser";
// // import url from "./model/db.js";

// const app = express();


// app.use(cors());
// app.use(bodyParser.json({ extended: true}));
// app.use(bodyParser.urlencoded({extended:true}));


// app.use('/users',route);
// const PORT = 4000;
// const URL = 'mongodb://User:usertask@crud-shard-00-00.iiunr.mongodb.net:27017,crud-shard-00-01.iiunr.mongodb.net:27017,crud-shard-00-02.iiunr.mongodb.net:27017/CRUD?ssl=true&replicaSet=atlas-vn4wg6-shard-0&authSource=admin&retryWrites=true&w=majority'


// mongoose.connect(URL,{ useNewUrlParser: true, useUnifiedTopology: true , useFindAndModify: false}).then(()=>{
//     app.listen(PORT,()=>{
//         console.log(`Server started on Port: ${PORT}`);
//     });
// }).catch(error => {
//     console.log('Error:', error.message)
// });

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const url = require('./model/db').url;
const cors = require('cors');
const bodyParser = require('body-parser')
const route = require('./route/routes')

app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors());
 app.use('/users',route);
 app.use('/auth',require('./route/auth.js'));


mongoose.set("useFindAndModify",false);
mongoose.connect(url,{useNewUrlParser:true,
    useUnifiedTopology:true})
    .then(()=>console.log('MongoDB connected'))
    .catch(err=>{
    console.log(err)
    });
const PORT = 4000;

// app.use('/auth',require('./routes/auth.js'));

app.listen(PORT,()=>{
    console.log(`Server started at ${PORT}`);
});