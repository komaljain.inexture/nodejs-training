// import mongoose from 'mongoose';
// import autoInc from 'mongoose-auto-increment';
const mongoose = require('mongoose');
const autoInc = require('mongoose-auto-increment');

const userSchema = mongoose.Schema({
    name: {
       type: String,
     required: true
    },
    username: {
        type: String,
     required: true
    },
    email:{
        type: String,
     required:true
    },
    password: {
        type: String,
        required:true
    },
    phone: {
        type: Number,
        required:true
    }
});
autoInc.initialize(mongoose.connection);
userSchema.plugin(autoInc.plugin,"user")
const user = mongoose.model("user",userSchema);

// export default user;
module.exports = user;