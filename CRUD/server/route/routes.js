// import express from "express";

// import { addUser, getUsers, getUserById ,editUser, deleteUser} from "../controller/userController.js";
const express = require('express');
const{ addUser, getUsers, getUserById ,editUser, deleteUser} = require('../controller/userController')

const route = express.Router();

route.get('/',getUsers);
route.post('/add',addUser);
route.get('/:id',getUserById);
route.put('/:id',editUser);
route.delete('/:id',deleteUser);

// export default route;
module.exports = route;