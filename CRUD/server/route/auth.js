
const express = require("express");
const router = express.Router();
const userModel = require("../model/loginSchema");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");



function isAuthenticated(req, res, next) {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, "secret", (err, user) => {
      if (!user) return res.json({ message: "User is not authenticated" });
      else {
        next();
      }
    });
  }
}

router.get("/protected", isAuthenticated, async (req, res) => {
 return res.json({ message: "This is a protected route " });


});

router.post("/signup", async (req, res) => {
  const { username, password } = req.body.data;
  console.log(req.body);

  if (!username || !password)
    return res.json({ message: "Invalid Credentials" });
  const userExists = await userModel.findOne({ username: username });

  if (userExists) return res.json({ message: "User already exists" });
  else {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const newUser = new userModel({
      username,
      password: hashPassword,
    });
    newUser.save();
    return res.json({ message: "User Created", newUser });
  }
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body.data;

  if (!username || !password)
    return res.json({ message: "Invalid Credentials" });

  const user = await userModel.findOne({ username: username });

  if (user) {
    const matchPassword = await bcrypt.compare(password, user.password);
    if(matchPassword){
      const payload = {
        username,
      };
      jwt.sign(payload, "secret", { expiresIn: "1d" }, (err, token) => {
        if (err) console.log(err);
        else {
          res.cookie("jwt", token);
          return res.json({ message: "User logged In", token: token });
        }
      });
    } else {
      return res.json({ message: "Incorrect Password" });
    }
  } else {
    return res.json({ message: "Incorreact Credentials" });
  }
});

module.exports = router;